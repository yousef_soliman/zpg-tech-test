var webpack = require('webpack');
var path = require('path');

var moduleLoaders = [
  // Babel transpile to ES5
  {
    test: /\.jsx?$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    options: {
      presets: ['react', 'es2015', 'stage-2']
    }
  },
  // Support for CSS Modules
  {
    test: /\.css$/,
    use: ['style-loader', 'css-loader']
  }

];

module.exports = {
  devtool: 'source-map',
  entry: [
      'babel-polyfill',
      './src/index.js'
  ],
  output: {
    path: path.join(__dirname, 'assets'),
    filename: 'bundle.js',
    publicPath: 'assets/'
  },
  resolve: {
    modules: ['node_modules', 'src/'],
    extensions: ['.js']
  },
  module: {
    rules: moduleLoaders
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  devServer: {
    port: 3031,
    hotOnly: true
  }
}