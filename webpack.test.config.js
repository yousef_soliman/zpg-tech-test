var webpack = require('webpack');
var path = require('path');

var moduleLoaders = [
  // Babel transpile to ES5
  {
    test: /\.jsx?$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    options: {
      presets: ['react', 'es2015', 'stage-2']
    }
  },
  // Support for CSS Modules
  {
    test: /\.css$/,
    use: ['style-loader', 'css-loader']
  }

];

module.exports = {
  devtool: 'inline-source-map',
  entry: [
      'babel-polyfill',
      'mocha-loader!./src/mocha-test-context.js'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: 'tests/'
  },
  resolve: {
    modules: ['node_modules', 'src'],
    extensions: ['.js'],
    alias: {
      'sinon': 'sinon/pkg/sinon'
    }
  },
  module: {
    rules: moduleLoaders
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  externals: {
    // Using Enzyme with Webpack 2 @ https://github.com/airbnb/enzyme/blob/master/docs/guides/webpack.md
    'cheerio': 'window',
    'react/addons': 'react',
    'react/lib/ExecutionEnvironment': 'react',
    'react/lib/ReactContext': 'react'
  },
  devServer: {
    port: 3032,
    hot: true
  },
  node: {
    fs: 'empty'
  }
}