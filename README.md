## Demo for React Router


## Usage

Clone down the repository and then run the following commands locally within the cloned folder:

### Installing Dependencies

There's a load of fun dependencies to download into node_modules/ so go ahead and run the following:

```
npm install
```

### Building the project

Run the following command locally in your CLI:

```
npm run build
```

Open your browser and visit the following URL:

```
http://localhost:3000/search
```

### Test cases

Run the following command locally in your CLI:

```
npm run build:test-client
```

Open your browser and visit the following URL:

```
http://localhost:3032/test.html
```

