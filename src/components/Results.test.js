import React from 'react'
import { expect } from 'chai'
import { shallow, mount } from 'enzyme'

import { Results, ResultsItem } from './Results'
import stub from '../../stub.json'

const mockListings = stub.listing

describe('React Component: <Results>', () => {

  const component = mount(<Results listings={ mockListings }/>)

  it('Will mount', () => {
    expect(component).to.not.be.undefined
  })

  it('Given a prop of listings <Array>, length of listings will equal number of child <ResultsItem> components', () => {
    expect(component.find(ResultsItem)).to.have.length(mockListings.length)
  })

  it('Contains number of results found with the number equal to the length of listings', () => {
    let expectedText = `${mockListings.length} results found`
    expect(component.find('.resultsCount').text()).to.equal(expectedText)
  })
})

describe('React Component: <ResultsItem>', () => {

  let mockListingItem = mockListings[0]

  const component = mount(<ResultsItem { ...mockListingItem } />)

  it('Will mount', () => {
    expect(component).to.not.be.undefined
  })

  it('Receives as listing prop an object containing all keys/values from the API listing schema', () => {
    expect(component.props()).to.eql(mockListingItem)
  })

  // Create property title, also test for result containing title
  it('The title should render as text in the format: "number of bedrooms" bedroom "property type" for sale', () => {
    let expectedText = `${mockListingItem.num_bedrooms} bedroom ${mockListingItem.property_type} for sale`
    expect(component.find('.listingTitle')).to.have.length(1)
    expect(component.find('.listingTitle').text()).to.equal(expectedText)
  })
  // Create agent address, also test for result containing agent address
  it('The agent address should render as text in the format: "agent address", "agent postcode"', () => {
    let expectedText = `${mockListingItem.agent_address}, ${mockListingItem.agent_postcode}`
    expect(component.find('.listingAgentAddress')).to.have.length(1)
    expect(component.find('.listingAgentAddress').text()).to.equal(expectedText)

  })


  // Format price, also test for result containing price
  it('The price should render as text in the format: "£x,xxx,xxx"', () => {
    component.setProps({ price: '1800000' })
    let expectedText = '£1,800,000'
    expect(component.find('.listingPrice').text()).to.equal(expectedText)
  })

  it('Contains property image', () => {
    expect(component.find('img.listingImage')).to.have.length(1)
    expect(component.find('img.listingImage').node.src).to.equal(mockListingItem.image_url)
  })

  it('Contains agent logo', () => {
    expect(component.find('img.listingAgentLogo')).to.have.length(1)
    expect(component.find('img.listingAgentLogo').node.src).to.equal(mockListingItem.agent_logo)
  })

  it('Contains agent name', () => {
    expect(component.find('.listingAgentName')).to.have.length(1)
    expect(component.find('.listingAgentName').text()).to.equal(mockListingItem.agent_name)
  })

  it('The agent phone number should render as text in the format: T: "agent phone number:"', () => {
    let expectedText = `T: ${mockListingItem.agent_phone}`
    expect(component.find('.listingAgentTelephone')).to.have.length(1)
    expect(component.find('.listingAgentTelephone').text()).to.equal(expectedText)
  })
})