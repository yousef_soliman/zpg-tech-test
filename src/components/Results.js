import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
/**
 * ResultsItem is a functional component used for each item in a collection of results.
 */
export const ResultsItem = (props) => {

  const format = {
    price: (price) => {
      // Format with comma separation according to locale, will work for locale en-GB, en-US, etc
      let formattedValue = parseInt(price).toLocaleString()
      return `£${formattedValue}`
    },
    title: (bedroomCount, propertyType) => {
      return `${bedroomCount} bedroom ${propertyType} for sale` 
    },
    agentAddress: (address, postCode) => {
      return `${address}, ${postCode}`
    },
    agentTelephone: (telephoneNumber) => {
      return `T: ${telephoneNumber}`
    }
  }

  const render = (props) => {
    return (
      <div className="listingItem section">
        <div className="col colFortyPct"><img className="listingImage" src={props.image_url}/></div>
        <div className="listingMainContent col colFortyPct">
          <h2 className="listingTitle">{ format.title(props.num_bedrooms, props.property_type) }</h2>
          <p className="listingPrice">{ format.price(props.price) }</p>
          <p className="listingDescription">{props.description}</p>
        </div>
        <div className="col colTwentyPct">
          <div className="listingAgentInfo">
            <img className="listingAgentLogo" src={props.agent_logo} />
            <p className="listingAgentName">{props.agent_name}</p>
            <p className="listingAgentAddress">{ format.agentAddress(props.agent_address, props.agent_postcode) }</p>
            <p className="listingAgentTelephone">{ format.agentTelephone(props.agent_phone) }</p>
          </div>
        </div>
      </div>
    )
  }

  return render(props)
}

/**
 * Results is the higher order component for the results page.
 */
export class Results extends React.Component {
  constructor(props) {
    super(props)
  }
  renderRedirect() {
    return (<Redirect to="/"/>)
  }
  renderListings(listings) {
    return (
      <div className="container">
      <p className="resultsCount">{listings.length} results found</p>
      <div>{listings.map((listing, key) => <ResultsItem { ...listing } key={ key } />) }</div>
      </div>
    )
  }
  render() {
    let { listings } = this.props
    return(
      <div>
      { listings.length === 0 && this.renderRedirect() }
      { listings.length > 0 && this.renderListings(listings) }
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    listings: state.data.listings
  }
}

function mapDispatchToProps (dispatch) {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Results)