import React from 'react'
import { expect } from 'chai'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'

import { Search } from './Search'


// Props to be passed through with connect() from react-redux,
// Mock props passed here for pure component testing
const props = {
  isGetListingsFulfilled: true,
  listings: [],
  getListings: sinon.spy()
}

describe('React Component: <Search>', () => {

  let component = mount(<Search {...props} />)

  it('Will mount', () => {
    expect(component).to.not.be.undefined
  })

  it('Contains page heading with text rendered as "Search for houses and flats for sale across the UK"', () => {
    expect(component.find('.pageTitle')).to.have.length(1)
    expect(component.find('.pageTitle').text()).to.equal('Search for houses and flats for sale across the UK')
  })

  it('Contains a text input and submit button', () => {
    expect(component.find('input.searchInput')).to.have.length(1)
    expect(component.find('input.searchSubmit')).to.have.length(1)
  })

  it('When input value is changed, state.value of component is updated', () => {
    let inputText = component.find('input.searchInput')
    inputText.simulate('change', { target: { value: 'N11' } })
    expect(component.state('value')).to.equal('N11')
  })

  it('When submit button is clicked, provided onSubmit function passed as prop is called', () => {
    let inputSubmit = component.find('input.searchSubmit')
    inputSubmit.simulate('click')

    expect(props.getListings.called).to.be.true
  })

  // Test case for if the length of store.results.listings is 0, render "No results found message"
  // Handled by Search component, value initialised as null
  it('If prop listings is an empty array, display message "No results found" and "Enter another location and search again."', () => {
    // Set props value of listins to an empty array
    component.setProps({ isGetListingsFulfilled: false })

    expect(component.find('.errorMessage')).to.have.length(1)
    expect(component.find('.pageTitle')).to.have.length(1)

    expect(component.find('.errorMessage').text()).to.equal('Enter another location and search again.')
    expect(component.find('.pageTitle').text()).to.equal('No results found')
  })
})