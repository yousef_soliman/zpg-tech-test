import React from 'react'
import { connect } from 'react-redux'
import { getListings } from '../actions/listings'

export class Search extends React.Component {
  state = {}
  render() {
    const { isGetListingsFulfilled } = this.props
    return( 
      <div className="container">
        <h2 className="pageTitle">{ isGetListingsFulfilled ? 'Search for houses and flats for sale across the UK' : 'No results found' }</h2>
        { !isGetListingsFulfilled && <p className="errorMessage">Enter another location and search again.</p> }
        <div className="section group">
          <div className="col colSeventyPct"><input type="text" className="searchInput" onChange={(event) => { this.setState({ value: event.target.value }) }} /></div>
          <div className="col colThirtyPct"><input type="submit" className="searchSubmit" value="Search" onClick={() => { this.props.getListings(this.state.value) }} /></div>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return state.data
}

function mapDispatchToProps (dispatch) {
  return {
    getListings: (value) => {
      dispatch(getListings(value))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)