import React from 'react'
import { Route } from 'react-router'
import Search from './../components/Search'
import Results from './../components/Results'

export const SearchRoute = <Route exact path="/search" component={Search}/>
export const ResultsRoute = <Route path="/results" component={Results}/>
export const NotFoundRoute = <Route render={() => <p>Requested page not found.</p>}/>