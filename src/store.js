/**
 * This file configures the Redux store and exports it.
 */
import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import thunk from 'redux-thunk'
// React Router Redux
import createHistory from 'history/createBrowserHistory'
import { routerReducer, routerMiddleware } from 'react-router-redux'
// Reducers
import { listings as listingsReducer } from './reducers/listings'

// History
export const history = createHistory()

/**
 * Get the middleware required according to the environment
 * @return {[type]} [description]
 */
const getMiddlewares = (env) => {
  let middlewares = [thunk, routerMiddleware(history)]
  //If not production, add logger to middleware and support for Chrome Redux DevTools
  if ('production' !== env) {
    const { logger } = require('redux-logger')
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    middlewares.push(logger)

    return composeEnhancers(applyMiddleware(...middlewares))
  }

  return applyMiddleware(...middlewares)
}

// Combine our app's reducer as prop data with routerReducer for storing react-router-redux state
const reducers = combineReducers({data: listingsReducer, router: routerReducer})

export const store = createStore(reducers, getMiddlewares(process.env.NODE_ENV))