import { expect } from 'chai'
import { listings as listingsReducer } from './listings'
import { GET_LISTINGS_FULFILLED, GET_LISTINGS_REJECTED } from '../actions/listings.actiontypes'

const initialState = {isGetListingsFulfilled: true, listings: []}

describe('Redux Reducer: Listings', () => {
  it('Initial state is an object with listings: [], isGetListingsFulfilled: true', () => {
    expect(listingsReducer()).to.eql(initialState)
  })

  it('Given action type GET_LISTINGS_FULFILLED, new state should reflect action.listings and isGetListingsFulfilled: true', () => {
    let action = {type: GET_LISTINGS_FULFILLED, listings: [{title: 'Test Property'}]}
    expect(listingsReducer(initialState, action)).to.eql({isGetListingsFulfilled: true, listings: action.listings})
  })

  it('Given action type GET_LISTINGS_REJECTED, new state should reflect isGetListingsFulfilled: false and listings: []', () => {
    let action = {type: GET_LISTINGS_REJECTED}
    expect(listingsReducer(initialState, action)).to.eql({isGetListingsFulfilled: false, listings: []})
  })
})