import { GET_LISTINGS_FULFILLED, GET_LISTINGS_REJECTED } from '../actions/listings.actiontypes'

export const listings = (state = { isGetListingsFulfilled: true, listings: [] }, action = {}) => {
  switch (action.type) {
    case GET_LISTINGS_FULFILLED:
      return { isGetListingsFulfilled: true, listings: action.listings }
    case GET_LISTINGS_REJECTED:
      return { isGetListingsFulfilled: false, listings: [] }
  }
  return state
}