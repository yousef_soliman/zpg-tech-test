/**
 * This file configures the Redux store for the server and exports it.
 */
import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import thunk from 'redux-thunk'
// React Router Redux
import { routerReducer } from 'react-router-redux'
// Reducers
import { listings as listingsReducer } from './reducers/listings'


// Combine our app's reducer as prop data with routerReducer for storing react-router-redux state
const reducers = combineReducers({data: listingsReducer, router: routerReducer})


export const serverStore = createStore(reducers)
