import { expect } from 'chai'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'
import fetchMock from 'fetch-mock'

import { GET_LISTINGS_FULFILLED, GET_LISTINGS_REJECTED } from './listings.actiontypes'
import { getListings, getListingsFulfilled, getListingsRejected } from './listings'

import { listings as listingsReducer } from './../reducers/listings'

const mockStore = configureStore([thunk])
const listingsResponse = [{ title: 'Test Property' }]

// const { mock } = fetchMock

describe('Redux Actions: Listings', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('Calling getListingsFulfilled({listings: [...]}) returns correct action object', () => {
    let action = {type: GET_LISTINGS_FULFILLED, listings: listingsResponse}

    const store = mockStore(listingsReducer())

    store.dispatch(getListingsFulfilled(listingsResponse))
    expect(store.getActions()[0]).to.eql(action)
   
  })

  it('Calling getListingsRejected() returns correct action object', () => {
    expect(getListingsRejected()).to.eql({type: GET_LISTINGS_REJECTED})
  })

  it('Calling getListings(<Area>) dispatches call to getListingsFulfilled() if fulfilled', () => {
    // Intercept network requests to address below, mock response HTTP status 200, body === listingsResponse
    fetchMock.mock('/query?area=N11', {
      status: 200,
      body: listingsResponse
    })

    // Expected actions to be dispatched on store
    const actions = [{type: GET_LISTINGS_FULFILLED, listings: listingsResponse}]
    // Setup mock store with listingsReducer() for initial state
    // console.log(listings, getListings)
    const store = mockStore(listingsReducer())
    // Expect call to store.getActions() === actions
    return store.dispatch(getListings('N11')).then(() => {
      expect(store.getActions()[0]).to.eql(actions[0])
    })
  })

  it('Calling getListings(<Area>) dispatches call to getListingsRejected() if rejected', () => {
    fetchMock.mock('/query?area=SE1', {
      status: 500
    })

    const actions = [{type: GET_LISTINGS_REJECTED}]
    const store = mockStore(listingsReducer())

    return store.dispatch(getListings('SE1')).then(() => {
      expect(store.getActions()).to.eql(actions)
    })
  })
})