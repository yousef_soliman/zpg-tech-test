import 'isomorphic-fetch'
import { push } from 'react-router-redux'
import { GET_LISTINGS_FULFILLED, GET_LISTINGS_REJECTED } from './listings.actiontypes'

export const getListingsFulfilled = (listings) => {
  return (dispatch) => {
    dispatch({type: GET_LISTINGS_FULFILLED, listings})
    return dispatch(push('/results'))
  }
}

// Could refactor to {{ type: GET_LISTINGS_REJECTED }}
export const getListingsRejected = () => {
  return { type: GET_LISTINGS_REJECTED }
}

export const getListings = (area) => {
  return (dispatch) => {
    return fetch(`/query?area=${area}`)
    .then(response => {
      if (response.ok) { 
        return response.json()
      } else {
        throw new Error()
      }
    })
    .then(json => dispatch(getListingsFulfilled(json)))
    .catch(err => {
      //console.log(err); // Don't swallow those errors yet please
      return dispatch(getListingsRejected())
    })
  }
}

/**
 * Action creator for a fetch() fulfilled with a 200 OK response
 * @param  {object} results Provided dataset
 * @return {object}         Action
 */
export const submitFulfilled = (results, push) => {
  push('/results')
  return {
    type: SEARCH_STATE_NAMESPACE + 'submitFulfilled',
    results
  }
}

/**
 * Action creator for a fetch() rejected
 * @return {object} Action, with empty array for listings prop on result object.
 */
export const submitRejected = () => {
  return {
    type: SEARCH_STATE_NAMESPACE + 'submitRejected'
  }
}

/**
 * Action creator with thunk that dispatches either submitFufilled() or submitRejected() based on promise end state
 * @param  {string} areaValue Postcode supplied by user input
 * @return {}           [description]
 */
export const submit = (areaValue, push) => {
  // Use redux-thunk in order to dispatch inside an action creator
  return (dispatch) => {
    // Return a promise with error handling, server sends back 500 if areaValue isn't present as area property in dataset
    return fetch(`/query?area=${areaValue}`).then(response => {
      if (response.ok) {
        return response.json()
      } else {
        throw new Error()
      }
    })
    .then(json => {
      return dispatch(submitFulfilled(json, push))
    })
    .catch((err) => { 
      return dispatch(submitRejected())
    })
  }
}
