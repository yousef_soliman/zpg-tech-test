import React from 'react'
import { render } from 'react-dom'

// import { BrowserRouter as Router, Route } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'
import { Route, Switch } from 'react-router'
import { Provider } from 'react-redux'

import { store, history } from './store'

import { SearchRoute, ResultsRoute, NotFoundRoute } from './routes/routes'

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        { SearchRoute }
        { ResultsRoute }
        { NotFoundRoute }
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
)