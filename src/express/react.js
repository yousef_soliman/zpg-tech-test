export const render = (jsx, preloadedState) => {
  return `
    <!DOCTYPE html>
      <html lang="en">
      <head>
      <meta charset="UTF-8">
      <title>Zoopla Test</title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.css"/>
      <link rel="stylesheet" href="assets/global.css"/>
      </head>
      <body>
      <div id="app">${jsx}</div>
      <script>window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}</script>
      <script src="assets/bundle.js"></script>
      </body>
    </html>
  `
}