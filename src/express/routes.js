import express from 'express'
import path from 'path'
import ReactDOMServer from 'react-dom/server'
import { StaticRouter, Route, Switch } from 'react-router'
import { Provider } from 'react-redux'
import { serverStore as store } from './../store-server'
import { SearchRoute, ResultsRoute, NotFoundRoute } from './../routes/routes'
import { render } from './react'
import stub from './../../stub.json'
import React from 'react'
// Assign new Express instance to app const
const app = express()

/**
* [description]
* @param  {[type]} '(^/search$|^/results$)' [description]
* @param  {[type]} (req,                    res           [description]
* @return {[type]}                          [description]
*
*/

app.use('/assets', express.static(path.join(__dirname, '../../assets')))

app.use((req, res, next) => {
  if (req.path.indexOf('query') !== -1) {
    if (req.query.area === 'N11') {
      res.send(stub.listing)
    } else {
      res.status(500).send()
    }
  } else {
    const context = {}
    const html = ReactDOMServer.renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={context}>
        <Switch>
          { SearchRoute }
          { ResultsRoute }
          <Route render={({staticContext}) => { staticContext.status = 404; return NotFoundRoute }}/>
        </Switch>
        </StaticRouter>
      </Provider>
    )

    let status = context.status || 200
    res.status(status).send(render(html, {}))
  }
  
})



// Export app (no app.listen call in this file for testing purposes)
export default app